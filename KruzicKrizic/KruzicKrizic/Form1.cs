﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KruzicKrizic
{
    public partial class Form1 : Form
    {
        Pen redP = new Pen(Color.Red, 2);
        Pen blueP = new Pen(Color.Blue, 2);
        Graphics p1, p2, p3, p4, p5, p6, p7, p8, p9;
        int bp1 = 1, bp2 = 1, bp3 = 1, bp4 = 1, bp5 = 1, bp6 = 1, bp7 = 1, bp8 = 1, bp9 = 1;//prikazuje je li crtano u tim picturebox-ovima 1 ako nije 0 ako je 
        int bro = 0;//brojaci rezultata
        int brx =0;
        int o1 =0, o2=0, o3=0, o4=0, o5=0, o6=0, o7=0, o8=0, o9=0;//kruzici
        int x1=0, x2=0, x3=0, x4=0, x5=0, x6=0, x7=0, x8=0, x9=0;//iksici
        int o = 0,x=0;//dodatne varijable za pomoc pri odredivanju u slucaju nerijesenog 
        int turn = 0;



        public Form1()
        {
            InitializeComponent();
            p1 = pictureBox1.CreateGraphics();
            p2 = pictureBox2.CreateGraphics();
            p3 = pictureBox3.CreateGraphics();
            p4 = pictureBox4.CreateGraphics();
            p5 = pictureBox5.CreateGraphics();
            p6 = pictureBox6.CreateGraphics();
            p7 = pictureBox7.CreateGraphics();
            p8 = pictureBox8.CreateGraphics();
            p9 = pictureBox9.CreateGraphics();
            lb_o.Text = bro.ToString();
            lb_x.Text = brx.ToString();
        }

        private void bt_1_Click(object sender, EventArgs e) //newgame button
        {
            p1.Clear(Color.White);
            p2.Clear(Color.White);
            p3.Clear(Color.White);
            p4.Clear(Color.White);
            p5.Clear(Color.White);
            p6.Clear(Color.White);
            p7.Clear(Color.White);
            p8.Clear(Color.White);
            p9.Clear(Color.White);
            pictureBox1.Enabled = true;
            pictureBox2.Enabled = true;
            pictureBox3.Enabled = true;
            pictureBox4.Enabled = true;
            pictureBox5.Enabled = true;
            pictureBox6.Enabled = true;
            pictureBox7.Enabled = true;
            pictureBox8.Enabled = true;
            pictureBox9.Enabled = true;
            turn = 0;
            x = 0;
            o = 0;
            o1 = 0; o2 = 0; o3 = 0; o4 = 0; o5 = 0; o6 = 0; o7 = 0; o8 = 0; o9 = 0;
            x1 = 0; x2 = 0; x3 = 0; x4 = 0; x5 = 0; x6 = 0; x7 = 0; x8 = 0; x9 = 0;
            bp1 = 1; bp2 = 1; bp3 = 1; bp4 = 1; bp5 = 1; bp6 = 1; bp7 = 1; bp8 = 1; bp9 = 1;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp1==1)
            {
                if (turn % 2 == 0)
                {
                    p1.DrawEllipse(redP, pictureBox1.Size.Width / 2 - 70 / 2, pictureBox1.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o1 = 1;
                }
                else
                {
                    p1.DrawLine(blueP, 30, 30, 90, 70);
                    p1.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x1 = 1;
                }
                bp1--;
                pobjeda();
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp2==1)
            {
                if (turn % 2 == 0)
                {
                    p2.DrawEllipse(redP, pictureBox2.Size.Width / 2 - 70 / 2, pictureBox2.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o2 = 1;
                }
                else
                {
                    p2.DrawLine(blueP, 30, 30, 90, 70);
                    p2.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x2 = 1;
                }
                bp2--;
                pobjeda();
            }
            
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp3==1)
            {
                if (turn % 2 == 0)
                {
                    p3.DrawEllipse(redP, pictureBox3.Size.Width / 2 - 70 / 2, pictureBox3.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o3 = 1;
                }
                else
                {
                    p3.DrawLine(blueP, 30, 30, 90, 70);
                    p3.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x3 = 1;
                }
                bp3--;
                pobjeda();
            } 
        }

        private void pictureBox7_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp7 == 1)
            {
                if (turn % 2 == 0)
                {
                    p7.DrawEllipse(redP, pictureBox7.Size.Width / 2 - 70 / 2, pictureBox7.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o7 = 1;
                }
                else
                {
                    p7.DrawLine(blueP, 30, 30, 90, 70);
                    p7.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x7 = 1;
                }
                bp7--;
                pobjeda();
            }
            
        }

        private void pictureBox8_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp8==1)
            {
                if (turn % 2 == 0)
                {
                    p8.DrawEllipse(redP, pictureBox8.Size.Width / 2 - 70 / 2, pictureBox8.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o8 = 1;
                }
                else
                {
                    p8.DrawLine(blueP, 30, 30, 90, 70);
                    p8.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x8 = 1;
                }
                bp8--;
                pobjeda();
            }
            
        }

        private void pictureBox9_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp9==1)
            {
                if (turn % 2 == 0)
                {
                    p9.DrawEllipse(redP, pictureBox9.Size.Width / 2 - 70 / 2, pictureBox9.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o9 = 1;

                }
                else
                {
                    p9.DrawLine(blueP, 30, 30, 90, 70);
                    p9.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x9 = 1;
                }
                bp9--;
                pobjeda();
            }
        }

        private void pictureBox6_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp6==1)
            {
                if (turn % 2 == 0)
                {
                    p6.DrawEllipse(redP, pictureBox6.Size.Width / 2 - 70 / 2, pictureBox6.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o6 = 1;
                }
                else
                {
                    p6.DrawLine(blueP, 30, 30, 90, 70);
                    p6.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x6 = 1;
                }
                bp6--;
                pobjeda();
            }
            
        }

        private void pictureBox5_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp5==1)
            {
                if (turn % 2 == 0)
                {
                    p5.DrawEllipse(redP, pictureBox5.Size.Width / 2 - 70 / 2, pictureBox5.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o5 = 1;
                }
                else
                {
                    p5.DrawLine(blueP, 30, 30, 90, 70);
                    p5.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x5 = 1;
                }
                bp5--;
                pobjeda();
            }
            
        }

        private void pictureBox4_MouseUp(object sender, MouseEventArgs e)
        {
            if (bp4==1)
            {
                if (turn % 2 == 0)
                {
                    p4.DrawEllipse(redP, pictureBox4.Size.Width / 2 - 70 / 2, pictureBox4.Size.Height / 2 - 70 / 2, 70, 70);
                    turn++;
                    o4 = 1;
                }
                else
                {
                    p4.DrawLine(blueP, 30, 30, 90, 70);
                    p4.DrawLine(blueP, 30, 70, 90, 30);
                    turn++;
                    x4 = 1;
                }
                bp4--;
                pobjeda();
            }
        }
        void pobjeda()
        {
            if (o1 * o2 * o3 == 1 || o4 * o5 * o6 == 1 || o7 * o8 * o9 == 1 || o1 * o4 * o7 == 1 || o2 * o5 * o8 == 1 || o3 * o6 * o9 == 1 || o1 * o5 * o9 == 1 || o3 * o5 * o7 == 1)
            {
                MessageBox.Show("Kruzic je pobjedio");
                o++;
                bro++;
                zakljucaj();
                
            }
            else if(x1 * x2 * x3 == 1 || x4 * x5 * x6 == 1 || x7 * x8 * x9 == 1 || x1 * x4 * x7 == 1 || x2 * x5 * x8 == 1 || x3 * x6 * x9 == 1 || x1 * x5 * x9 == 1 || x3 * x5 * x7 == 1)
            {
                MessageBox.Show("Iksic je pobjedio");
                x++;
                brx++;
                zakljucaj();
                
            }
            if(turn==9 && o==0 && x==0)
            {
                MessageBox.Show("Nema pobjednika, nerijeseno");
            }
        }
        void zakljucaj()
        {
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox4.Enabled = false;
            pictureBox5.Enabled = false;
            pictureBox6.Enabled = false;
            pictureBox7.Enabled = false;
            pictureBox8.Enabled = false;
            pictureBox9.Enabled = false;
            lb_o.Text = bro.ToString();
            lb_x.Text = brx.ToString();


        }
    }
}


